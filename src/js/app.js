import $ from 'jquery';
import Snap from 'snapsvg-cjs';
import {onOpenMenu, prevStep, nextStep} from './lib/helpers';
import 'slick-carousel';
import './lib/imgLightbox.js';

/* img lightbox */
$( function()
{
  if($('.wrapper-news').length) {
    $('body').append('<div id="overlay-lightbox"></div>');
  }
  $( '.lightbox' ).imageLightbox({
    // selector:       'id="imagelightbox"',   // string;
    // allowedTypes:   'png|jpg|jpeg|gif',     // string;
    // animationSpeed: 250,                    // integer;
    // preloadNext:    true,                   // bool;            silently preload the next image
    // enableKeyboard: true,                   // bool;            enable keyboard shortcuts (arrows Left/Right and Esc)
    // quitOnEnd:      false,                  // bool;            quit after viewing the last image
    // quitOnImgClick: false,                  // bool;            quit when the viewed image is clicked
    // quitOnDocClick: true,                   // bool;            quit when anything but the viewed image is clicked
    // onStart:        false,                  // function/bool;   calls function when the lightbox starts
    // onEnd:          false,                  // function/bool;   calls function when the lightbox quits
    // onLoadStart:    false,                  // function/bool;   calls function when the image load begins
    // onLoadEnd:      false                   // function/bool;   calls function when the image finishes loading

    onStart: function() {
      $('body').addClass('open-lightbox');
    },
    onEnd: function() {
      $('body').removeClass('open-lightbox');
    }
  });
});

// super menu
const $overlay = $('#menu');


if($overlay.length) {
  const steps = $overlay.attr('data-steps').split(';'),
    stepsTotal = steps.length;


  function toggleOverlay() {

    if ($overlay.hasClass('is-open')) {

      //close menu
      prevStep({
        path: Snap.select('.overlay-path'), 
        pos: stepsTotal - 1, 
        steps,  
        $target: $overlay
      });

    } else {

      //open menu
      onOpenMenu($overlay);
      nextStep({
        path: Snap.select('.overlay-path'), 
        pos: 0, 
        steps,
        stepsNb: stepsTotal - 1
      });

    }
  }
  $(document).on('click', '.overlay-trigger, .overlay-close', function(e) {
    e.preventDefault();
    toggleOverlay();
  });
}

/* add class to header when scroll*/
$(window).scroll(function() {
  var navbar = $('header.header');
  if ($(this).scrollTop() > 10) {
    navbar.addClass('is-sticky');
  }
  else{
    navbar.removeClass('is-sticky');
  }
});

// slider
let $slider = $('.slider-card');
$slider.slick({
  dots: false,
  arrows: true,
  prevArrow: '<button class="btn-back btn-slider btn-slider__prev"><i class="icon icon-back"></i></button>',
  nextArrow: '<button class="btn-back btn-slider btn-slider__next"><i class="icon icon-back"></i></button>',
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});



// Объект для которого будет применён эффект
$('.is-rippled').click(function(e) {
  var ripple = $(this);
  // визуальный элемент эффекта
  if (ripple.find('.effekt').length === 0) {
    ripple.append("<span class='effekt'></span>");
  }
  var efekt = ripple.find('.effekt');
  efekt.removeClass('replay');
  if (!efekt.height() && !efekt.width()) {
    var d = Math.max(ripple.outerWidth(), ripple.outerHeight());
    efekt.css({
      height: d / 5,
      width: d / 5
    }); // Определяем размеры элемента эффекта 
  }
  var x = e.pageX - ripple.offset().left - efekt.width() / 2;
  var y = e.pageY - ripple.offset().top - efekt.height() / 2;
  efekt.css({
    top: y + 'px',
    left: x + 'px' 
  }).addClass('replay');
});


$('.form-control').on('input propertychange', function() {
  let $parent = $(this).closest('.form-control__wrapper');
  let self = $(this);
  if(self.val().trim()) {
    $parent.addClass('is-filled');
  } else {
    $parent.removeClass('is-filled');
  }
});

// custom social share btn
$(window).bind('load', function() { 
       
  var stickHeight = 0,
    stickF = 0,
    stickG = 0,
    $elem = $('[data-sticky-role="elem"]'),
    $wrap = $('[data-sticky-role="parent_elem"]');
      
  positionStick();
  
  function positionStick() {
  
    stickHeight = $elem.height();
    stickF = $(window).scrollTop() - $wrap.height();
    stickG = stickF + $(window).height()+ stickHeight - 270;

    if (stickG > 0) {
      $elem.removeClass('is-sticky');
    } else {
      $elem.addClass('is-sticky');
    }
      
          
  }

  $(window)
    .scroll(positionStick)
    .resize(positionStick);
          
});

